export PATH := $(PATH):bin
SRCS := $(sort $(wildcard *.md))
SRCS := $(filter-out README.md, $(SRCS))
DEST := $(notdir $(PWD))

.PHONY: static
static: output/index.html
	@if [ -d static ]; then cp -rpL static/ output/; fi

%.html: %.md
	[ -f requirements.txt ] && pip -q install -r requirements.txt
	sed 's/#!//e;' $< | mdtoreveal /dev/stdin --output $@

output/index.md: $(SRCS)
	mkdir -p output
	cat $(SRCS) > output/index.md

.PHONY: rsync
rsync: static
	rsync -ah --delete output/ mdk@mdk.fr:/var/www/mdk.fr/$(DEST)/

.PHONY: clean
clean:
	rm -fr output .cache

.PHONY: entr
entr:
	ls -1 *.md | entr $(MAKE) static

.PHONY: serve
serve: static
	python3 -m http.server -d output/

.PHONY: test
test:
	pytest
