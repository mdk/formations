#!/bin/sh

if ! which cutycapt >/dev/null
then
    echo Please apt install cutycapt
    exit 1
fi

if ! which pytest >/dev/null
then
    echo Please pip install -r requirements.txt
    exit 1
fi

set -e  # So a breaking test stops the deploy.

export QT_QPA_PLATFORM=offscreen

for makefile in */Makefile
do
    dir="$(dirname "$makefile")"
    figlet -C utf8 -t "$dir"
    (
        cd "$dir"

        make test
        make rsync
    )
done
