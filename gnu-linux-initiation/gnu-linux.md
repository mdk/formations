# GNU/Linux avec Debian

### Présenté par

<!-- .slide: data-background="static/background.jpg" -->

Julien Palard <julien@palard.fr>


https://mdk.fr

::: notes

Introduce yourself!


# Avant de commencer

## Qui utilise GNU/Linux ?

- 500 des 500 plus gros supercalculateurs,
- 80% des serveurs exposés sur Internet,
- 42% des clients accédant à Internet,
- les télévisions, montres, voitures, l'ISS,
- Google, Amazon, Facebook, Apple, Wikipedia,
- la gendarmerie, le parlement, le ministère de l'agriculture…


# Les environnements de bureau

## KDE

<img src="https://upload.wikimedia.org/wikipedia/commons/d/d9/KDE_1.0.png" style="width: 70%"/>

## KDE

![](https://upload.wikimedia.org/wikipedia/commons/a/a9/KDE_Plasma_5.16.png)

## GNOME

<video controls=1 src="https://static.gnome.org/release/42/appearance.webm"/>

## Phosh

<img src="https://upload.wikimedia.org/wikipedia/commons/6/6f/Phosh_on_PinePhone.png" style="width: 30%"/>


# Le shell

```shell
mdk@seraph$ date
Thu May 25 03:37:25 PM CEST 2023

mdk@seraph$ cal
      May 2023
Su Mo Tu We Th Fr Sa
    1  2  3  4  5  6
 7  8  9 10 11 12 13
14 15 16 17 18 19 20
21 22 23 24 25 26 27
28 29 30 31
```

## Les commandes

On exécute des « commandes » ou des « programmes ».

Ces programmes peuvent prendre des « arguments » ou des « paramètres ».


## Le shell

Dans :

```shell
$ date
```

- `$ ` est « le prompt ».
- `date` est un programme.

notes:

Oui le prompt est modifiable, je veux dire qu'on ne peut pas l'effacer
avec backspace.


## Le shell

Dans :

```shell
$ man intro
```

- `man` est un programme.
- `intro` est un `argument` qui lui est donné.


## Le shell

Mais pourquoi !?


## Le shell

- Ça permet de faire collaborer des programmes.
- Il est facile d'ajouter des programmes.
- Tout au clavier, c'est plus rapide.
- Une séquence de commandes peut être partagée et rejouée, c’est « un script ».


## C'est plus rapide

- Dû à la simplicité de l'interface.
- L'auto-complétion des commandes.
- Les raccourcis claviers.
- La quantité de programmes disponnibles (~4500 chez moi, là).

notes: démo ! Ctrl-r !


## Kit de survie

- `pwd`
- `cd`
- `ls`
- `cp`, `mv`
- `rm`
- `mkdir`

notes: Il n'y a pas de corbeille !


## Kit de survie

- `less`, `see`, `edit`
- `man`
- `grep`
- `find`


# La philosophie Unix

> Write programs that do one thing and do it well.

Notes:

L'idée ici est d'éviter de dupliquer du code. Ne l'écrire qu'une fois, mais bien.

Un exemple typique : la recherche dans un fichier.

Dans un contexte unix, un seul outil sert à chercher du texte dans un fichier : `grep`.

Dans les autres contextes, chaque programme ré-implémente à sa manière
le code nécessaire à la recherche de texte dans un fichier :

- Firefox
- Microsoft Word
- Chromium
- Thunderbird
- Microsoft Excel
- Libreoffice
- SublimeText
- Visual Studio Code

C'est de nombreuses heures de perdues, à réimplémenter inlassablement
la même fonctionalité.


## La philosophie Unix

> Write programs to work together.

notes:
Pour pouvoir réutiliser un programme il faut qu'il s'interface
correctement avec les autres :

- Un programme doit s'exprimer dans un dialecte compréhensible pour un autre.
- Un programme doit comprendre facilement le dialecte d'un autre.


## La philosophie Unix

> Write programs to handle text streams, because that is a universal interface.

notes:
Parler de l'omniprésence du texte brut (de Markdown à HTTP/1.1 en
passant par les fichiers source), en opposition aux formats binaires
(sortis d'éditeurs comme Word, ...).  (Je ne veux plus voir des
débutants ouvrir Microsoft Word pour taper leurs premières lignes de
code.)

Digrésser sur Markdown et reStructuredText, mentionner groff.


# Le shell, démos

Voici les programmes que j'utilise le plus :

```shell
$ history | awk '{print $2}' | sort | uniq -c | sort -gr
3408 git
902 cd
765 python
539 ls
385 emacs
330 curl
327 ssh
291 make
209 mv
188 rm
...
```


## Démo

Pour jouer un peu avec le shell, on va utiliser [Open Food
Facts](https://www.data.gouv.fr/fr/datasets/open-food-facts-produits-alimentaires-ingredients-nutrition-labels/#resource-164c9e57-32a7-4f5b-8891-26af10f91072).

C'est un fichier de produits alimentaires.


## Démo

Téléchargeons et décompressons le fichier :

```shell
$ curl -O https://fr.openfoodfacts.org/data/fr.openfoodfacts.org.products.csv.gz
$ gunzip fr.openfoodfacts.org.products.csv.gz
$ mv fr.openfoodfacts.org.products.csv products.csv
```

Attention, compressé il fait ~700M, décompressé ~8GB.


## Démo

Peser le fichier :

```shell
$ du products.csv
8103812	products.csv
```

Ahh oui, 8 millions d'octets…


## Démo

Plus **h**umain :

```shell
$ du -h products.csv
7.8G	products.csv
```


## Démo

Compter les lignes :

```shell
$ wc -l products.csv
2864834 products.csv
```


## Démo

Que contient-il ?

```shell
$ file products.csv
products.csv: UTF-8 Unicode text, with very long lines
```


## Démo

Lire le fichier :

```shell
$ less products.csv
```

Attention le fichier est gigantesque !


## Démo

Obtenir les 10 premières lignes du fichier :

```shell
$ head products.csv
code	url	creator	created_t	created_datetime ... ...
00000000000000225	http://world-fr.openfoodfacts.org/produit/00000000000000225/jeunes-pousses-endives...
0000000000000207025004	http://world-fr.openfoodfacts.org/produit/0000000000000207025004/andre...
...
...
```

## Les noms des colonnes


```shell
$ head -n 1 products.csv | tr '\t' '\n'
code
url
creator
created_t
created_datetime
```

## Le nombre de colonnes

```shell
$ head -n 1 products.csv | tr '\t' '\n' | wc -l
201
```

Parmis les colonnes je repère `additives`…


## Les numéros des colonnes

```shell
$ head -n 1 products.csv | tr '\t' '\n' | grep -n additives
51:additives_n
52:additives
53:additives_tags
54:additives_fr
```

## Les additifs

```shell
$ cut -f 51-54 products.csv | less
```

Je repère « E412 », je me demande si beaucoup de produits l'utilisent…


## Les additifs

```shell
$ cut -f 51-54 products.csv | grep -c E412
44554
```

Je me demande quel est l'additif le plus utilisé…


## Les additifs

```shell
$ cut -f 51-54 products.csv |
> grep -oE 'E[0-9]{3}' | sort | uniq -c | sort -gr | head
 196667 E322
 153420 E330
 114926 E500
  61884 E160
  56411 E415
  54712 E450
  49713 E471
  44554 E412
  44121 E202
  42398 E150
```


# Le système de fichiers

> Tout est fichier.

```shell
$ ls /
bin   home            lib32       media  root  sys  vmlinuz
boot  initrd.img      lib64       mnt    run   tmp
dev   initrd.img.old  libx32      opt    sbin  usr
etc   lib             lost+found  proc   srv   var
```

ou

```shell
$ man hier
```


## Même l'alimentation électrique

```shell
$ cat /sys/class/power_supply/AC/online
1
```

## Les droits



## Les points de montage

# ssh

SSH: Parler de clefs, conseiller ED25519

```
eval $(ssh-agent -s)
ssh-add
```

Aborder les clefs physiques + Fido U2F


## sed, c'est bien



## dig, et comment fonctionne le DNS

```
# récupérer la zone root (et oui, il faut connaître un serveur root
# pour trouver un serveur root... les résolveurs DNS les connaissent
# en fait plus ou moins "tous", ils ne changent pas tous les matins.)
$ dig @198.41.0.4 . NS +norec

# Disons qu'on veut trouver la machine qui s'appelle `cr.yp.to` :
# Demander à un serveur root, c'est le seul qu'on connaît :
dig @192.5.5.241 cr.yp.to +norec
# Il nous donne une réponse contenant les NS de `to.`, ainsi que leurs IPs.

# On demande donc à un de ces NS la même question :
dig @216.74.32.101 cr.yp.to +norec
# Qui nous répond avec la liste des NS pour yp.to, ainsi que leurs IPs.

# On demande donc à un de ces NS la même question :
dig @131.193.32.108 cr.yp.to +norec
# Qui nous donne (enfin !) une réponse A (IPv4)

# Si quelqu'un demande pourquoi le NS de yp.to. à un nom à
# ralonge, c'est que c'est du CurveDNS (...de la crypto).
```

Parler des enregistrements TXT rapidement pendant qu'on y est.

Aussi du cache, pour démonter des préjugés comme "il faut forcément
attendre un temps fou après avoir configuré un enregistrement".

Parler des DNS menteurs, et de Google Chrome qui flood les racines en
tentant de détecter les DNS menteurs.


## curl, wget

Parler de HTTP/1.1, si possible leur faire une démo à la main, avec
l'IP récupérée précédement de cr.yp.to :

```
$ netcat 131.193.32.108 80
GET / HTTP/1.1
Host: cr.yp.to

```
```
HTTP/1.1 200 OK
```

Digrésser sur la sémantique HTTP.


## vim, emacs, nano

Donner en passant les raccourcis de base de bash et d'emacs :

- `ctrl-a` (début de ligne)
- `ctrl-e` (fin de ligne)
- `ctrl-z` (suspend, parler de `fg`, `bg`, `jobs`)
 - `ctrl-k` (kill-line)


## git

Clarifier rapidement git vs guthub : j'aime dire « git est à github ce
qu'un jpg est à dropbox » pour leur faire comprendre que git n'est
*pas* Github.

- Qu'est-ce qu'un commit, comment sont-ils rangés.
- Qu'est-ce qu'un remote, qu'est-ce qu'un clone, (et qu'est-ce qu'un fork ?).
- Qu'est-ce qu'une branche, qu'est-ce qu'un merge, qu'est-ce qu'un rebase.
- git bisect
- git grep


## apt install, pip install, ...

Peut être sans trop comparer avec une install sur Windows (chercher
sur Google, tomber sur un site louche, télécharger un malware), mais
tout de même, montrer la puissance de la chose :

    apt install vlc
    vlc cybersdf/

Parler du côté extrêmemnent controllé des paquets des distribs, en
opposition au côté extrêmement yolo des paquets des langages (npm,
pip, le typo squatting, supply-chain attacks, ...).


## rsync

On ne parle plus de `scp`, `scp` c'est finit, RIP.

Aborder rsnapshot si possible, digresser sur l'importance des
sauvegardes.


## L'encodage

Mettre en opposition "glyphe" (sur un coin de nappe en papier) et
"bits" (sur un disque ou par du cuivre), pour vite ammener au côté
nécessaire d'encoder d'un côté et de décoder de l'autre (pour le
transport, le stockage, ...).

Digresser sur le binaire, l'hexadécimal, leur rappeler les méthodes de
conversion au moins jusqu'a l'octet.

Digresser rapidement sur la logique booléenne :

  - 0 and x ?
  - 1 or x ?
  - not(a or b) == ?
  - (not a) or (not b)


Parler d'encodage de caractères, peut être en partant d'ASCII (pas
avant, ce n'est pas utile et ils ne retiendront pas tout), Unicode,
UTF-8, UTF-16, UTF-32, BE, LE, le BOM.

Quelques démos s'imposeront avec `iconv` et peut être Python.


# TODO

- Le shebang
- Les alias, les fonctions bash.
- Redirections.
- Petit historique
- Les window-managers
- Sweet sweet thinks like youtube-dl
- crons / crontabs
- débugger (journalctl, /var/log)
- https://www.lopezferrando.com/30-interesting-shell-commands/
