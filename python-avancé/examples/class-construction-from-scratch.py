# This is the creation « from scratch » of the following class:

# class Foo:
#     def say(self):
#         print("hello")

# First the type.__prepare__ is called to create a namespace, it
# typically returns a fresh empty dict:

ns = {}

# Then the class body is executed in this namespace:

exec(
    """
def say(self):
    print("hello")
""",
    globals(),
    ns,
)

# Now, the type __new__ is called to create the class:

Foo = type.__new__(type, "Foo", (), ns)  # ~500 lines of C, actual malloc()
type.__init__(Foo, "Foo", (), ns)  # Does nothing

Foo().say()
