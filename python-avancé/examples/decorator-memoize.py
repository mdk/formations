def memoize(limit): # Factory
    def _memoize(function_fib):
        memory = {}
        def cached_fib(n):
            if n in memory:
                return memory[n]
            result = function_fib(n)
            memory[n] = result
            return result
        return cached_fib
    return _memoize

@memoize(limit=1024)
def fib(n):
    if n < 2:
        return 1
    return fib(n-1) + fib(n-2)

# fib = memoize(limit=1024)(fib)
