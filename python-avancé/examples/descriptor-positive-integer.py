class PositiveInteger:
    def __set_name__(self, owner, name):
        self.name = name

    def __get__(self, instance, cls=None):
        if instance is None:
            return self
        return instance.__dict__[self.name]

    def __set__(self, instance, value):
        if value < 0:
            raise ValueError("Negative values are not acceptable.")
        instance.__dict__[self.name] = value


class Order:
    qty = PositiveInteger()
    price = PositiveInteger()

    def __init__(self, item, price, qty):
        self.item = item
        self.price = price
        self.qty = qty

    def cost(self):
        return self.price * self.qty


# Order("un truc", -1, 1)
# o = Order("Un livre", 35, 1)
# o.qty += 1
# o.qty -= 10
# print(o.cost())
