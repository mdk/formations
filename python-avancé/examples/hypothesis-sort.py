from string import ascii_letters

from hypothesis import given, settings

from hypothesis.strategies import text, lists


@given(lists(text(alphabet=ascii_letters, max_size=10)))
@settings(max_examples=500)
def test_sort_is_stable(a_list):
    print(a_list)
    double_sort = sorted(sorted(a_list, key=str.lower), key=len)
    single_sort = sorted(a_list, key=lambda s: (len(s), s.lower()))
    assert double_sort == single_sort
