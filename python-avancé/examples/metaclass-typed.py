class StronglyTypedMeta(type):
    def __new__(cls, name, bases, ns, **kwds):
        if '__annotations__' in ns:
            for name, type in ns['__annotations__'].items():
                ns[name] = StronglyTypedDescriptor(name, type)
        return super().__new__(cls, name, bases, ns, **kwds)


class StronglyTypedDescriptor:
    def __init__(self, name, type):
        self.name = name
        self.type = type

    def __get__(self, instance, cls=None):
        return instance.__dict__[self.name]

    def __set__(self, instance, value):
        if not isinstance(value, self.type):
            raise TypeError(f"{self.name} should be of type {self.type}.")
        instance.__dict__[self.name] = value



class Test(metaclass=StronglyTypedMeta):
    x: str
    y: int
