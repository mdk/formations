def introspected_method(clsname, name, method):
    def _(*args, **kwargs):
        print(f"Calling {clsname}.{name}(*{args!r}, **{kwargs!r})...")
        returning =  method(*args, **kwargs)
        print(f"    returned {returning!r}")
        return returning

    return _


class Introspected(type):
    def __new__(cls, name, bases, ns, **kwds):
        for key, value in ns.items():
            if isinstance(value, classmethod):
                ns[key] = classmethod(introspected_method(name, key, value.__wrapped__))
            elif callable(value) and key != "__repr__":
                ns[key] = introspected_method(name, key, value)
        return super().__new__(cls, name, bases, ns, **kwds)


class Metaclass(type, metaclass=Introspected):
    @classmethod
    def __prepare__(metacls, name, bases, **kwds):
        return super().__prepare__(metacls, name, bases, **kwds)

    def __new__(cls, name, bases, ns, **kwds):
        return super().__new__(cls, name, bases, ns, **kwds)

    def __init__(self, name, bases, ns, **kwds):
        super().__init__(name, bases, ns, **kwds)


class Point(metaclass=Metaclass):
    def __init__(self, x, y):
        self.x = x
        self.y = y
