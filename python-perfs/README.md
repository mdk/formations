# Python Performances

Les slides : https://mdk.fr/python-perfs


## Description

Destinée aux développeurs Python aguerris, cette formation approche
les différents moyens d’améliorer les performances d’un programme
écrit en Python.


## Les objectifs

- Savoir mesurer les performances d’un programme et identifier les goulots d’étranglement.
- Prendre conscience des impacts des différentes structures de données, de leur complexité algorithmique.
- Découvrir les différents compilateurs (*Just-In-Time* et *Ahead-Of-Time*) de l’écosystème Python.
- Découvrir la variété des interpréteurs Python et leurs caractéristiques.
- Entrelacer du code natif et du Python.


## Pré-requis

- Excellente connaissance du langage, du niveau de la formation « Python avancé ».


## Programme

Bien choisir sa structure de donnée :

- comparaison asymptotique,
- comprendre les implémentations `O(1)`.

Les outils de mesure :

- Les outils extérieurs à Python (`time`, `hyperfine`, …).
- Configurer sa machine pour avoir des mesures reproductibles.
- Les outils de la bibliothèque standard (`cProfile`, `pstats`, `timeit`).
- Les outils tiers (`pyperf`, `snakeviz`, `Scalene`, …).

Les JIT, compilateurs, et interpréteurs tiers :

- PyPy, numba, cython, pythran, nuitka, mypyc, pyston, …

Utiliser du code natif pour optimiser ponctuellement :

- Interfacer du C ou du C++ avec Python en utilisant cython.
- Rédiger un module Python en C.


## Mise en pratique

- Rédaction naïve d’un algorithme (modèle du tas de sable abélien).
- Étude de la complexité, du temps d’exécution, et des goûlots d’étranglements des implémentations.
- Implémenter des optimisations ciblées par les mesures précédentes.
- Tentative d’utiliser des structures de données spécialisées.
- Tentatives d’utiliser différents compilateurs (JIT et Ahead of Time) et interpréteurs.
