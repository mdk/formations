// Compile with:
// cc -c -fPIC my_collatz.c -o libcollatz.so

#include <string.h>


static long collatz(long n) {
    if (n % 2 == 0)
        return n / 2;
    else
        return n * 3 + 1;
}

#define CACHE_SIZE 1024
static long cache[CACHE_SIZE];

void clear_cache(void) {
    memset(cache, 0, CACHE_SIZE * sizeof(long));
}


static long flight_duration(long n) {
    long result;

    if (n == 1)
        return 0;
    if (n < 1024 && cache[n] != 0)
        return cache[n];
    result = 1 + flight_duration(collatz(n));
    if (n < 1024)
        cache[n] = result;
    return result;
}

long next_flight(long duration) {
    long new_duration;

    for (long i = 1; ; ++i) {
        new_duration = flight_duration(i);
        if (new_duration > duration)
            return i;
    }
}
