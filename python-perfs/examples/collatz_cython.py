from functools import cache
from itertools import count


def collatz(n):
    if n % 2 == 0:
        return n // 2
    else:
        return n * 3 + 1


@cache
def flight_duration(n):
    if n == 1:
        return 0
    return 1 + flight_duration(collatz(n))


def next_flight(duration):
    for i in count(1):
        if flight_duration(i) > duration:
            return i
