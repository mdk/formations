import sys
from functools import cache
from itertools import count


def collatz(n):
    if n % 2 == 0:
        return n // 2
    else:
        return n * 3 + 1


_cache = {}

def flight_duration(n):
    steps = 0
    while n != 1:
        n = collatz(n)
        steps += 1
        if n in _cache:
            return _cache[n] + steps
    return steps


def next_flight(duration):
    for i in count(1):
        new_duration = flight_duration(i)
        _cache[i] = new_duration
        if new_duration > duration:
            return i
