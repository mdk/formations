from itertools import count

from numba import njit


@njit
def flight_duration(n):
    if n == 1:
        return 0
    if n % 2 == 0:
        return 1 + flight_duration(n // 2)
    else:
        return 1 + flight_duration(n * 3 + 1)



def next_flight(duration):
    for i in count(1):
        if flight_duration(i) > duration:
            return i
