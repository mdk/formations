from itertools import count


#pythran export capsule collatz(uint64)
def collatz(n):
    if n % 2 == 0:
        return n // 2
    else:
        return n * 3 + 1


#pythran export capsule flight_duration(uint64, uint64:uint64 dict)
def flight_duration(n, cache):
    if n in cache:
        return cache[n]
    result = 1 + flight_duration(collatz(n), cache)
    cache[n] = result
    return result


#pythran export next_flight(uint64)
def next_flight(duration):
    cache = {1: 0}
    for i in count(1):
        new_duration = flight_duration(i, cache)
        if new_duration > duration:
            return i
