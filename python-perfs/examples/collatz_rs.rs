// rustimport:pyo3

use std::sync::Mutex;
use pyo3::prelude::*;

fn collatz(n: u64) -> u64 {
    if n % 2 == 0 {
        n / 2
    }
    else {
        n * 3 + 1
    }
}

fn flight_duration(n: u64) -> u64 {
    static cache: Mutex<[u64;1024]> = Mutex::new([0; 1024]);

    if n == 1 {
        return 0;
    }
    if n < 1024 && cache.lock().unwrap()[n as usize] != 0 {
        return cache.lock().unwrap()[n as usize];
    }
    let result = 1 + flight_duration(collatz(n));
    if n < 1024 {
        cache.lock().unwrap()[n as usize] = result;
    }
    result
}

#[pyfunction]
fn next_flight(duration: u64) -> u64 {
    let mut new_duration: u64;
    let mut i = 1;
        
    loop {
        new_duration = flight_duration(i);
        if new_duration > duration {
            return i;
        }
        i += 1;
    }
}
