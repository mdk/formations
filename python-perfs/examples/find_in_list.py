from bisect import bisect_left


def reference_index(a_list, a_value):
    """Locate the leftmost value exactly equal to a_value.

    This one is from docs.python.org.
    """
    i = bisect_left(a_list, a_value)
    if i != len(a_list) and a_list[i] == a_value:
        return i
    raise ValueError


def index(a_list, a_value):
    """Locate the leftmost value exactly equal to a_value."""
    begin, end = 0, len(a_list)
    while begin < end:  # Search in a_list[begin:end]
        middle = (end + begin) // 2
        if a_value > a_list[middle]:
            begin = middle + 1
        else:
            end = middle
    if (a_list and begin != len(a_list) and
        a_list[begin] == a_value):
        return begin
    raise ValueError


def dumb_index(a_list, a_value):
    """Locate the leftmost value exactly equal to a_value."""
    for i, value in enumerate(a_list):
        if value == a_value:
            return i
    raise ValueError
