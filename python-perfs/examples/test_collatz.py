import subprocess
from importlib import import_module
from pathlib import Path

import pytest

from collatz import flight_duration


def test_collatz_length_according_to_wikipedia():
    assert flight_duration(15) == 17
    assert flight_duration(27) == 111


@pytest.mark.parametrize("file", [file.stem for file in Path("examples").glob("collatz_*.py")])
def test_collatz_file(file):
    if 'pythran' in file:
        subprocess.run(['pythran', f'examples/{file}.py'])
    if 'nuitka' in file:
        subprocess.run(['python', '-m', 'nuitka','--module', f'{file}.py'])
    if 'cython' in file:
        subprocess.run(['cythonize', '--inplace', f'examples/{file}.py'])

    module = import_module("examples." + file)
    assert module.next_flight(300) == 26623
