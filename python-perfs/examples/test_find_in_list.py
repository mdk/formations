import pytest
from hypothesis import given
from hypothesis.strategies import integers, lists

from find_in_list import dumb_index, index, reference_index


@pytest.mark.parametrize("function", [dumb_index, index, reference_index])
def test_index(function):
    assert function([1, 2, 3], 1) == 0
    assert function([1, 2, 3], 2) == 1
    assert function([1, 2, 3], 3) == 2

    with pytest.raises(ValueError):
        assert function([1, 2, 3], 1.1)
    with pytest.raises(ValueError):
        assert function([1, 2, 3], 2.1)
    with pytest.raises(ValueError):
        assert function([1, 2, 3], 3.1)



@given(lists(integers()), integers())
def test_hypo(l, i):
    l.sort()
    try:
        reference_index(l, i)
    except ValueError:
        value_error = True
    else:
        value_error = False
    if value_error:
        with pytest.raises(ValueError):
            index(l, i)
    else:
        assert reference_index(l, i) == index(l, i) == dumb_index(l, i)
