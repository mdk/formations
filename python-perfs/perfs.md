# Les performances en Python

par

<!-- .slide: data-background="static/background.jpg" -->

Julien Palard <julien@palard.fr>

https://mdk.fr


# Bien choisir sa structure de donnée

C'est bien choisir l'algorihtme qu'on va utiliser.


## Comparaison asymptotique

Les notations les plus utilisées :

```text
O(1)        Constant
O(log n)    Logarithmique
O(n)        Linéaire
O(n log n)  « Linéarithmique »
O(n²)       Quadratique
O(2ⁿ)       Exponentielle
O(n!)       Factorielle
```

notes:

Il faut les grapher pour s'en rendre compte : cf. examples/big.o.py


## Comparaison asymptotique

Exemples.


## O(1)

```python
def get_item(a_list: list, an_idx):
    return a_list[an_idx]
```

ou

```python
def is_in(a_set: set, a_value):
    return a_value in a_set
```

notes:

Peu importe la taille de la liste, accéder à un élément prend le même temps.


## O(log n)

Attention c'est toujours en base deux.

Exemple typique : chercher dans un annuaire.

notes:

Un annuaire deux fois plus gros ne vous demandera pas deux fois plus
de temps mais peut-être une opération de plus.


## O(log n)

```python
#!sed -n '/def index/,/^$/p' examples/find_in_list.py
```

## O(n)

```python
#!sed -n '/def dumb_index/,/^$/p' examples/find_in_list.py
```

## O(n log n)

C'est `n` fois un `log n`, par exemple rayer `n` personnes dans un annuaire.

Typique d'algorithmes de tris.


## Les mesures de complexité

- De temps (CPU consommé).
- D'espace (mémoire  consommée).
- Dans le meilleur des cas.
- Dans le pire des cas.
- Dans le cas moyen.
- Amorti.
- ...


## Les mesures de complexité

Il n'est pas forcément nécessaire d'apprendre par cœur toutes les complexités de chaque opération.

Pas toute suite.


## Les bases

Mais comprendre la complexité de quelques structures
élémentaires permet d'éviter les « erreurs de débutants ».


## Rappel des unités de temps

- 1 milliseconde (1 ms) c'est un millième de seconde.
- 1 microseconde (1 μs) c'est un millionième de seconde.
- 1 nanoseconde (1 ns) c'est un milliardième de seconde.


## Rappel des unités de temps

- milli c'est `10 ** -3`, c'est `0.001`.
- micro c'est `10 ** -6`, c'est `0.000_001`.
- nano c'est `10 ** -9`, c'est `0.000_000_001`.


## Le cas typique

```shell
#!cache python -m pyperf timeit --quiet --setup 'container = list(range(10_000_000))' '10_000_001 in container'

#!cache python -m pyperf timeit --quiet --setup 'container = set(range(10_000_000))' '10_000_001 in container'
```

Pourquoi une si grande différence !?


notes:

C'est l'heure du live coding !


# À vous !

Simulez un tas de sable, moi je joue avec la conjecture de Syracuse.

Ne vous souciez pas des perfs, on s'en occupera.

notes:

Leur laisser ~15mn.

voir: https://git.afpy.org/mdk/fast-abelian-sandpile/


## Simulateur de tas de sable

Rédigez une fonction nommée `apply_gravity`, acceptant un `array
numpy` à deux dimensions :
- chaque valeur de l’array est un entier représentant le nombre de
grains de sables empilés verticalement à cet endroit.
- une colonne de 4 grains de sable ou plus s’effondre sous l’effet de
la gravité : un grain part au nord, un autre au sud, un à l’est, et un à
l’ouest.

## Exemple

```python
>>> import numpy as np
>>> area = np.zeros((5, 5), dtype=np.uint32)
>>> area[2, 2] = 16  # Une colonne de 16 grains au milieu
>>> apply_gravity(area)
>>> print(area)  # La colonne de 16 grains s’est effondrée
[[0 0 1 0 0]
 [0 2 1 2 0]
 [1 1 0 1 1]
 [0 2 1 2 0]
 [0 0 1 0 0]]
```

# Les outils

notes:

Je mesure mes perfs, puis ils mesurent leurs perfs.


## `pyperf command`

```shell
#!cache pyperf command python examples/collatz.py 100

#!cache pyperf command python examples/collatz.py 200

#!cache pyperf command python examples/collatz.py 300
```

## Petite parenthèse

Mais attention, démarrer un processus Python n'est pas gratuit :

```shell
#!cache pyperf command python -c pass
```

notes:

N'essayez pas de retenir les chiffres, retenez les faits.


## Petite parenthèse

Et puis il peut dépendre de la version de Python, des options de
compilation, ... :

```shell
$ pyperf command ~/.local/bin/python3.10 -c pass
.....................
command: Mean +- std dev: 37.6 ms +- 0.6 ms

$ pyperf command /usr/bin/python3.10 -c pass
.....................
command: Mean +- std dev: 14.4 ms +- 0.4 ms
```

notes:

Leur parler de `--enable-optimizations` (PGO).


## `pyperf timeit`

Il existe aussi `timeit` dans la stdlib, mais je préfère `pyperf timeit` :

```shell
#!cache pyperf timeit --quiet --setup 'from examples.collatz import next_flight' 'next_flight(200)'

#!cache pyperf timeit --quiet --setup 'from examples.collatz import next_flight' 'next_flight(300)'
```


## Les outils — À vous !

Effectuez quelques mesures sur votre implémentation.

Tentez d'en déterminer la complexité en fonction du nombre de grains.

Explorez les limites de vos implémentations.


# Profilage

`pyperf` c'est bien pour mesurer, comparer.

Le profilage peut nous aider à trouver la fonction coupable.


## cProfile, exemple

```python
#!cat examples/collatz.py
```


## cProfile, exemple

Sortons cProfile :

```shell
$ python -m cProfile --sort cumulative collatz.py 300
#!cache python -m cProfile --sort cumulative examples/collatz.py 300 | sed 1d
```

## cProfile, exemple

Cachons les résultats de `flight_duration` :
```python
#!sed -n '/@cache/,/^$/p' examples/collatz_cache.py
```


## cProfile, exemple

On mesure toujours :

```shell
#!cache pyperf timeit --quiet --setup 'from examples.collatz import next_flight' 'next_flight(300)'

#!cache pyperf timeit --quiet --setup 'from examples.collatz_cache import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'
```


## cProfile, exemple

Et on repasse dans cProfile voir ce qu'on peut améliorer :

```shell
#!cache python -m cProfile --sort cumulative examples/collatz_cache.py 300 | sed 's/collatz_cache.py:[0-9]*(\(.*\))/\1/g'
```

## Snakeviz

```shell
$ python -m pip install snakeviz
$ python -m cProfile -o .cache/collatz.prof collatz_cache.py 300
$ python -m snakeviz .cache/collatz.prof
```

#!if [ ! -f .cache/collatz_cache.prof ]; then python -m cProfile -o .cache/collatz_cache.prof examples/collatz_cache.py 300 >/dev/null 2>&1; fi
#!if [ ! -f output/collatz_cache-snakeviz.png ]; then python -u -m snakeviz -s .cache/collatz_cache.prof > .cache/snakeviz.stdout & TOKILL=$!; sleep 1; cutycapt --min-width=1024 --delay=500 --url="$(tail -n 1 .cache/snakeviz.stdout)" --out=output/collatz_cache-snakeviz.png ; kill $TOKILL; fi


## Snakeviz

![](collatz_cache-snakeviz.png)


## Scalene

```shell
$ python -m pip install scalene
$ scalene collatz_cache.py 400
```

#!if [ ! -f output/collatz_cache.html ]; then ( cd examples; scalene collatz_cache.py 400 --html --outfile ../output/collatz_cache.html --cli >&2 ); fi
#!if [ ! -f output/collatz_cache-scalene.png ]; then cutycapt --min-width=1024 --delay=100 --url=file://$(pwd)/output/collatz_cache.html --out=output/collatz_cache-scalene.png; fi


## Scalene

![](collatz_cache-scalene.png)


## line_profiler

```shell
$ python -m pip install line_profiler
#!cache python -m kernprof --view --prof-mod examples/collatz_cache.py --line-by-line examples/collatz_cache.py 300
```

## py-spy

```shell
$ pip install py-spy
#!if [ ! -f output/py-spy.svg ]; then py-spy record -o output/py-spy.svg -- python examples/collatz_cache.py 300; fi
```


## py-spy

![](py-spy.svg)


## Aussi

- https://github.com/gaogaotiantian/viztracer
- https://github.com/joerick/pyinstrument
- https://github.com/benfred/py-spy
- https://github.com/sumerc/yappi
- https://github.com/vmprof/vmprof-python
- https://github.com/bloomberg/memray
- https://github.com/pythonprofilers/memory_profiler


## Profilage — À vous !

Profilez votre implémentation et tentez quelques améliorations.


# Cython

Cython est un dialecte de Python transpilable en C.


## Cython démo

Sans modifier le code :

```bash
$ pip install cython
$ cythonize --inplace -3 examples/collatz_cython.py
#!CFLAGS=-03 cythonize --inplace -3 examples/collatz_cython.py
```

## Cython démo

```
#!cache pyperf timeit --quiet --setup 'from examples.collatz_cache import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'

#!cache pyperf timeit --quiet --setup 'from examples.collatz_cython import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'
```


## Cython démo

En annotant le fichier on permet à cython d'utiliser des types natifs.

Et ainsi réduire les aller-retour coûteux entre le C et Python.


## Cython annotate

```shell
#!cache cython -3 --annotate examples/collatz_annotated.py
#!if ! [ -f output/collatz_annotated.png ] ; then cutycapt --min-width=1024 --delay=500  --url=file://$(pwd)/examples/collatz_annotated.html --out=output/collatz_annotated.png; fi
```

![](collatz_annotated.png)


## Cython démo

En annotant le code :

```bash
$ cythonize --inplace examples/collatz_annotated.py
#!cythonize --inplace examples/collatz_annotated.py
```

## Cython démo

```
#!cache pyperf timeit --quiet --setup 'from examples.collatz_cython import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'

#!cache pyperf timeit --quiet --setup 'from examples.collatz_annotated import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'
```


## Cython — À vous !


# Numba

Numba est un `JIT` : « Just In Time compiler ».

```python
#!cat examples/collatz_numba.py
```

## Numba démo

```shell
#!cache pyperf timeit --quiet --setup 'from examples.collatz_cache import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'

#!cache pyperf timeit --quiet --setup 'from examples.collatz_numba import next_flight' 'next_flight(300)'
```

## numba — À vous !


# pypy

pypy est un interpréteur Python écrit en Python.

#!pypy3 -m venv .venvpypy
#!.venvpypy/bin/python -m pip -q install pyperf

```shell
#!cache pyperf timeit --quiet --setup 'from examples.collatz_cache import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'

#!cache .venvpypy/bin/pypy3 -m pyperf timeit --quiet --setup 'from examples.collatz_cache import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'
```


# mypyc

mypyc est un compilateur qui s'appuie sur les annotationes de type mypy :

```python
#!cat examples/collatz_mypy.py
```

## mypyc demo

```shell
$ pip install mypy
$ mypyc collatz_mypy.py
#!cd examples; mypyc collatz_mypy.py
```


## mypyc demo

```shell
#!cache pyperf timeit --quiet --setup 'from examples.collatz_cache import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'

#!cache pyperf timeit --quiet --setup 'from examples.collatz_mypy import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'
```

## mypyc — À vous !


# Pythran

pythran est un compilateur pour du code scientifique :

```python
#!cat examples/collatz_pythran.py
```

## Pythran demo

```shell
$ pip install pythran
$ pythran examples/collatz_pythran.py
#!if ! [ -f examples/collatz_pythran.*.so ]; then cd examples; pythran collatz_pythran.py; fi
```


## Pythran demo

```shell
#!cache pyperf timeit --quiet --setup 'from examples.collatz_cache import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'

#!cache pyperf timeit --quiet --setup 'from examples.collatz_pythran import next_flight' 'next_flight(300)'
```


## pythran — À vous !


# Nuitka

Aussi un compilateur, aussi utilisable pour distribuer une application.

```shell
$ pip install nuitka
$ python -m nuitka --module collatz_nuitka.py
#!if ! [ -f examples/collatz_nuitka.*.so ]; then (cd examples/; python -m nuitka --module collatz_nuitka.py >/dev/null); fi
```

```shell
#!cache pyperf timeit --quiet --setup 'from examples.collatz_cache import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'

#!cache pyperf timeit --quiet --setup 'from examples.collatz_nuitka import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'
```

# Voir aussi

- JAX


# Hand crafted C

```c
#!cat examples/collatz.c
```

Mais comment l'utiliser ?


## Avec Cython

```cpython
#!cat examples/collatz_cython_to_c.pyx
```

## Avec Cython

```shell
$ cythonize -i examples/collatz_cython_to_c.pyx
#!if ! [ -f examples/collatz_cython_to_c.*.so ] ; then cythonize -i examples/collatz_cython_to_c.pyx; fi
```

## Avec Cython

```shell
#!cache pyperf timeit --quiet --setup 'from examples.collatz_cache import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'

#!cache pyperf timeit --quiet --setup 'from examples.collatz_cython_to_c import next_flight' 'next_flight(300)'
```


# Hand crafted rust

```rust
#!cat examples/collatz_rs.rs
```


## with rustimport

```bash
$ pip install rustimport
```


## with rustimport

```bash
#!cache pyperf timeit --quiet --setup 'from examples.collatz_cache import next_flight, flight_duration' 'flight_duration.cache_clear(); next_flight(300)'

#!cache pyperf timeit --quiet --setup 'import rustimport.import_hook; import rustimport.settings; rustimport.settings.compile_release_binaries = True; from examples.collatz_rs import next_flight' 'next_flight(300)'
```
